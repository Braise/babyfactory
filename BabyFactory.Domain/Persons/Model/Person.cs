﻿using BabyFactory.Domain.Commun.Model;
using BabyFactory.Domain.Commun.Specification.Interface;
using BabyFactory.Domain.Commun.Specification.Validators;
using System;

namespace BabyFactory.Domain.Persons.Model
{
    public class Person
    {
        private readonly IValidation<Person> _validation;

        public string Nam { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public DateTime Birthday { get; set; }

        public Sex Sex { get; set; }

        public bool HasUndergoneSterilization { get; set; }

        public Person(
            string nam,
            string firstname,
            string lastname,
            DateTime birthday,
            Sex sex,
            bool hasUndergoneSterilization,
            IValidation<Person> validation)
        {
            Nam = nam;
            Firstname = firstname;
            Lastname = lastname;
            Birthday = birthday;
            Sex = sex;
            HasUndergoneSterilization = hasUndergoneSterilization;

            _validation = validation;
        }

        public ValidationResult Validate()
        {
            return _validation.Validate(this);
        }
    }
}
