﻿using BabyFactory.Domain.Commun.Specification.Model;
using BabyFactory.Domain.Commun.Specification.Validators;
using BabyFactory.Domain.Persons.Model;

namespace BabyFactory.Domain.Persons.Service
{
    public class PersonService : Validator<Person>
    {
        public PersonService()
        {
            Add(new Rule<Person>(new IsInAdulthood(), "Must be at least 18"));
        }

        public override ValidationResult Validate(Person entity)
        {
            return base.Validate(entity);
        }
    }
}
