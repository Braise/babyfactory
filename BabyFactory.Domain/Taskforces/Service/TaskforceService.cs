﻿using BabyFactory.Domain.Commun.Specification.Interface;
using BabyFactory.Domain.Commun.Specification.Model;
using BabyFactory.Domain.Commun.Specification.Validators;
using BabyFactory.Domain.Taskforces.Model;
using BabyFactory.Domain.Workers.Model;
using System.Collections.Generic;

namespace BabyFactory.Domain.Taskforces.Service
{
    public class TaskforceService : Validator<Taskforce>
    {
        private readonly IValidation<Worker> _workerValidation;

        public TaskforceService(IValidation<Worker> workerValidation)
        {
            _workerValidation = workerValidation;

            Add(new Rule<Taskforce>(new ContainsAtLeastOneWoman(), "Must contains at least one woman"));
            Add(new Rule<Taskforce>(new HasAtLeastOneValidDiagnostic(), "Must have at least one valid diagnostic"));
        }

        public override ValidationResult Validate(Taskforce taskforce)
        {
            List<ValidationResult> validations = new List<ValidationResult>();
            validations.Add(base.Validate(taskforce));

            foreach (var worker in taskforce.Workers)
            {
                validations.Add(_workerValidation.Validate(worker));
            }

            return ValidationResult.Construire(validations.ToArray());
        }
    }
}
