﻿using BabyFactory.Domain.Commun.Specification.Validators;
using BabyFactory.Domain.Persons.Model;

namespace BabyFactory.Domain.Taskforces.Interface
{
    public interface IPersonService
    {
        ValidationResult CheckEligibility(Person person);
    }
}
