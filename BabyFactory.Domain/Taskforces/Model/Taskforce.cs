﻿using BabyFactory.Domain.Commun.Specification.Interface;
using BabyFactory.Domain.Commun.Specification.Validators;
using BabyFactory.Domain.Workers.Model;
using System.Collections.Generic;

namespace BabyFactory.Domain.Taskforces.Model
{
    public class Taskforce
    {
        private readonly IValidation<Taskforce> _validation;

        public IEnumerable<Worker> Workers { get; set; }

        public Taskforce(IEnumerable<Worker> workers, IValidation<Taskforce> validation)
        {
            Workers = workers;
            _validation = validation;
        }

        public ValidationResult Validate()
        {
            return _validation.Validate(this);
        }
    }
}
