﻿using BabyFactory.Domain.Commun.Specification.Interface;
using BabyFactory.Domain.Commun.Specification.Model;
using BabyFactory.Domain.Commun.Specification.Validators;
using BabyFactory.Domain.Persons.Model;
using BabyFactory.Domain.Workers.Model;

namespace BabyFactory.Domain.Workers.Service
{
    public class WorkerService : Validator<Worker>
    {
        private readonly IValidation<Person> _personValidation;

        public WorkerService(IValidation<Person> personValidation)
        {
            _personValidation = personValidation;

            Add(new Rule<Worker>(new SterilizationWasNotVolontary(), "The sterilization must not be voluntary"));
        }

        public override ValidationResult Validate(Worker worker)
        {
            var workerValidation = base.Validate(worker);

            var personValidation = _personValidation.Validate(worker.Person);

            return ValidationResult.Construire(workerValidation, personValidation);
        }
    }
}
