﻿using BabyFactory.Domain.Commun.Specification.Interface;
using BabyFactory.Domain.Commun.Specification.Validators;
using BabyFactory.Domain.Persons.Model;

namespace BabyFactory.Domain.Workers.Model
{
    public class Worker
    {
        private readonly IValidation<Worker> _validation;

        public Person Person { get; set; }
        public bool SterilizationWasVoluntary { get; set; }
        public bool WasDiagnosedWithInfertility { get; set; }
        public bool WasDiagnosedWithInabilityToReproduce { get; set; }
        public bool HasParentalCapacity { get; set; }

        public Worker(
            Person person,
            bool sterilizationWasVoluntary,
            bool wasDiagnosedWithInfertility,
            bool wasDiagnosedWithInabilityToReproduce,
            bool hasParentalCapacity,
            IValidation<Worker> validation
        )
        {
            Person = person;
            SterilizationWasVoluntary = sterilizationWasVoluntary;
            WasDiagnosedWithInfertility = wasDiagnosedWithInfertility;
            WasDiagnosedWithInabilityToReproduce = wasDiagnosedWithInabilityToReproduce;
            HasParentalCapacity = hasParentalCapacity;

            _validation = validation;
        }

        public ValidationResult Validate()
        {
            return _validation.Validate(this);
        }
    }
}
