﻿using BabyFactory.Domain.Commun.Specification.Interface;
using BabyFactory.Domain.Persons.Model;
using System;

namespace BabyFactory.Domain.Commun.Specification.Model
{
    public class IsInAdulthood : ISpecification<Person>
    {
        public bool IsSatisfiedBy(Person entity)
        {
            // Save today's date.
            var today = DateTime.Today;

            // Calculate the age.
            var age = today.Year - entity.Birthday.Year;

            // Go back to the year in which the person was born in case of a leap year
            if (entity.Birthday.Date > today.AddYears(-age)) age--;

            return age >= 18;
        }
    }
}
