﻿using BabyFactory.Domain.Commun.Specification.Interface;
using BabyFactory.Domain.Workers.Model;

namespace BabyFactory.Domain.Commun.Specification.Model
{
    public class HasParentalCapacity : ISpecification<Worker>
    {
        public bool IsSatisfiedBy(Worker entity)
        {
            return entity.HasParentalCapacity;
        }
    }
}
