﻿using BabyFactory.Domain.Commun.Specification.Interface;
using BabyFactory.Domain.Workers.Model;

namespace BabyFactory.Domain.Commun.Specification.Model
{
    public class SterilizationWasNotVolontary : ISpecification<Worker>
    {
        public bool IsSatisfiedBy(Worker entity)
        {
            return !entity.SterilizationWasVoluntary;
        }
    }
}
