﻿using BabyFactory.Domain.Commun.Model;
using BabyFactory.Domain.Commun.Specification.Interface;
using BabyFactory.Domain.Taskforces.Model;
using System.Linq;

namespace BabyFactory.Domain.Commun.Specification.Model
{
    public class ContainsAtLeastOneWoman : ISpecification<Taskforce>
    {
        public bool IsSatisfiedBy(Taskforce entity)
        {
            return entity.Workers.Any(x => x.Person.Sex == Sex.Female);
        }
    }
}
