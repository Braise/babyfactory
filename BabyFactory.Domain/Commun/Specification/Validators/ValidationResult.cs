﻿using System.Collections.Generic;
using System.Linq;

namespace BabyFactory.Domain.Commun.Specification.Validators
{
    public class ValidationResult
    {
        private List<string> _errorMessages;

        public bool IsValid => !_errorMessages.Any();

        public IEnumerable<string> ErrorMessages => _errorMessages;

        public ValidationResult()
        {
            _errorMessages = new List<string>();
        }

        public static ValidationResult Construire(params ValidationResult[] validationResults)
        {
            var returnValidation = new ValidationResult();

            foreach (var validation in validationResults)
            {
                if (!validation.IsValid)
                {
                    returnValidation.AddErrorMessages(validation.ErrorMessages);
                }
            }

            return returnValidation;
        }

        public void AddErrorMessage(string errorMessage)
        {
            _errorMessages.Add(errorMessage);
        }

        private void AddErrorMessages(IEnumerable<string> errorMessages)
        {
            _errorMessages.AddRange(errorMessages);
        }
    }
}
