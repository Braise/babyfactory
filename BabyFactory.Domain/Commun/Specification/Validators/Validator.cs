﻿using BabyFactory.Domain.Commun.Specification.Interface;
using System.Collections.Generic;

namespace BabyFactory.Domain.Commun.Specification.Validators
{
    public class Validator<T> : IValidation<T>
    {
        private IList<Rule<T>> _rules;

        public Validator()
        {
            _rules = new List<Rule<T>>();
        }

        public void Add(Rule<T> rule)
        {
            _rules.Add(rule);
        }

        public virtual ValidationResult Validate(T entity)
        {
            var result = new ValidationResult();

            foreach (var rule in _rules)
            {
                if (!rule.Validate(entity))
                {
                    result.AddErrorMessage(rule.ErrorMessage);
                }
            }

            return result;
        }
    }
}
