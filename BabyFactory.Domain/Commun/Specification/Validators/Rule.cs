﻿using BabyFactory.Domain.Commun.Specification.Interface;

namespace BabyFactory.Domain.Commun.Specification.Validators
{
    public class Rule<T>
    {
        private readonly ISpecification<T> _specification;

        public string ErrorMessage { get; }

        public Rule(ISpecification<T> specification, string errorMessage)
        {
            _specification = specification;
            ErrorMessage = errorMessage;
        }

        public bool Validate(T entity)
        {
            return _specification.IsSatisfiedBy(entity);
        }
    }
}
