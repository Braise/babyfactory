﻿using BabyFactory.Domain.Commun.Specification.Validators;

namespace BabyFactory.Domain.Commun.Specification.Interface
{
    public interface IValidation<T>
    {
        ValidationResult Validate(T entity);
    }
}
