﻿namespace BabyFactory.Domain.Commun.Specification.Interface
{
    public interface ISpecification<T>
    {
        bool IsSatisfiedBy(T entity);
    }
}
