import { Sex } from "./Sex.enum";

export class Person {
    constructor(
        public Nam: string,
        public FirstName: string,
        public LastName: string,
        public Birthday: Date,
        public Sex: Sex,
        public HasUndergoneSterilization: boolean,
    ){}
}