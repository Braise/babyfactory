import { NewTaskForceComponent } from "../Components/new-task-force/new-task-force.component";
import { ErrorCase } from "./ErrorCase.model";
import { TaskForce } from "./TaskForce.model";

export class ResultatTaskForceValidation{
    TaskForce?: TaskForce;
    Errors?: ErrorCase[];

    get Success():boolean{
        return this.Errors === undefined || this.Errors.length === 0;
    }
}