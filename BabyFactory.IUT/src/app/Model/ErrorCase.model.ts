export class ErrorCase{

    constructor(public errorMessage: string, public nam?:string){

    }

    public toString():string{
        let message = "";

        if(this.nam){
            message = this.nam + " : ";
        }

        return message + this.errorMessage;
    }
}