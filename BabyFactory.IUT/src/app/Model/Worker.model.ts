import { Person } from "./Person.model";

export class Worker{
    constructor(
        public Person: Person,
        public SterilizationWasVoluntary: boolean,
        public WasDiagnosedWitInfertility: boolean,
        public WasDiagnosedWithInabilityToReproduce: boolean,
        public HasParentalCapacity: boolean
    ){}
}