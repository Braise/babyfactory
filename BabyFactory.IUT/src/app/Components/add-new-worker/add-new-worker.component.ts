import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Worker } from 'src/app/Model/Worker.model';
import { Person } from 'src/app/Model/Person.model';
import { PersonService } from 'src/app/Services/Person.service';
import { ErrorCase } from 'src/app/Model/ErrorCase.model';

@Component({
  selector: 'babyFactory-add-new-worker',
  templateUrl: './add-new-worker.component.html',
  styleUrls: ['./add-new-worker.component.css']
})
export class AddNewWorkerComponent implements OnInit {
  @Input()
  workerToModify?: Worker;

  @Output()
  newWorkerEvent = new EventEmitter<Worker>();

  displayWorkerForm:boolean = false;
  namForm?: FormGroup;
  //nam: FormControl = new FormControl('', [this.restrictedWords, this.obligatoryWords]);
  workerForm?: FormGroup;
  person?: Person
  errorCases: ErrorCase[] = [];
  
  constructor(
    public activeModal: NgbActiveModal,
    private personService: PersonService){
      this.namForm = new FormGroup({
        nam : new FormControl('', [this.restrictedWords(), this.obligatoryWords()])
      });
      this.workerForm = new FormGroup({
        hasUndergoneSterilization: new FormControl(undefined),
        sterilizationWasVoluntary : new FormControl(false),
        wasDiagnosedWitInfertility : new FormControl(false),
        wasDiagnosedWithInabilityToReproduce : new FormControl(false),
        hasParentalCapacity : new FormControl(false),
      });
    }

  ngOnInit(): void {
    if(this.workerToModify){
      this.displayWorkerForm = true;
      this.person = this.workerToModify.Person;
      this.workerForm!.controls.hasUndergoneSterilization.setValue(this.workerToModify.Person.HasUndergoneSterilization);
      this.workerForm!.controls.sterilizationWasVoluntary.setValue(this.workerToModify.SterilizationWasVoluntary);
      this.workerForm!.controls.wasDiagnosedWitInfertility.setValue(this.workerToModify.WasDiagnosedWitInfertility);
      this.workerForm!.controls.wasDiagnosedWithInabilityToReproduce.setValue(this.workerToModify.WasDiagnosedWithInabilityToReproduce);
      this.workerForm!.controls.hasParentalCapacity.setValue(this.workerToModify.HasParentalCapacity);
    }
  }

  private restrictedWords(): ValidatorFn {
    console.log('dans premier validateur');
    return (control: AbstractControl):{ [key: string]: any} => {
      var result = control.value.includes('foo') ? { 'restrictedWord': 'foo' } : null as any;
      console.log(result);
      return control.value.includes('foo') ? { 'restrictedWord': 'foo' } : null as any;
    }
  }

  private obligatoryWords(): ValidatorFn{
    return (control: AbstractControl):{ [key: string]: any} => {
      var result = control.value.includes('hello') ? null as any : { 'obligatoryWord': 'hello' };
      console.log(result);
      return control.value.includes('hello') ? null as any : { 'obligatoryWord': 'hello' };
    }
  }

  searchWorker(){
    console.log(this.namForm!.controls.nam.errors);
    this.personService
      .getPerson(this.namForm!.controls.nam.value)!
      .subscribe((persons: Person[]) => {
        if(!persons || !persons.length){
          this.errorCases?.push(new ErrorCase("Looks like this worker doesn't exist. Please let him know we are hiring!", this.namForm!.controls.nam.value));
          console.log(this.errorCases);
        }else{
          this.person = persons[0];
          this.workerForm!.controls.hasUndergoneSterilization.setValue(this.person?.HasUndergoneSterilization);
          this.displayWorkerForm = true;
        }
      });
  }

  addNewMember(){
    this.person!.HasUndergoneSterilization = this.workerForm!.controls.hasUndergoneSterilization.value;

    const member = new Worker(
      this.person!,
      this.workerForm!.controls.sterilizationWasVoluntary.value,
      this.workerForm!.controls.wasDiagnosedWitInfertility.value,
      this.workerForm!.controls.wasDiagnosedWithInabilityToReproduce.value,
      this.workerForm!.controls.hasParentalCapacity.value
    );
      
    this.newWorkerEvent.emit(member);
    this.activeModal.close();
  }
}
