import { Component, Input, OnInit } from '@angular/core';
import { CaptainHaddockService } from 'src/app/Services/CaptainHaddock.service';
import { ErrorCase } from '../../Model/ErrorCase.model';

@Component({
  selector: 'babyFactory-display-errors',
  templateUrl: './display-errors.component.html',
  styleUrls: ['./display-errors.component.css']
})
export class DisplayErrorsComponent{

  @Input()
  errorCases?: ErrorCase[]; 

  constructor(public captainHaddockService: CaptainHaddockService) { }

  public getErrorsToDisplay():string[]{
    console.log(this.errorCases);
    if(this.captainHaddockService.theCaptainIsWatching){
      return this.cursesToDisplay;
    }

    return this.businessErrorsToDisplay;
  }

  private get cursesToDisplay():string[]{
    let curses: string[] = [];

    for (let index = 0; index < this.errorCases!.length; index++) {
      curses.push(this.captainHaddockService.getCurses());
    }

    return curses;
  }

  private get businessErrorsToDisplay():string[]{
    let errors: string[] = [];

    for (const error of this.errorCases!) {
      errors.push(error.toString());
    }

    return errors;
  }
}
