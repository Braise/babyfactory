import { Component } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Worker } from 'src/app/Model/Worker.model';
import { AddNewWorkerComponent } from '../add-new-worker/add-new-worker.component';
import { faUserPlus, faUserMinus, faUserEdit } from '@fortawesome/free-solid-svg-icons';
import { ValidateTaskForceService } from 'src/app/Services/Validate-Task-Force.service';
import { ErrorCase } from 'src/app/Model/ErrorCase.model';

@Component({
  selector: 'babyFactory-new-task-force',
  templateUrl: './new-task-force.component.html',
  styleUrls: ['./new-task-force.component.css']
})
export class NewTaskForceComponent {

  isDirty: boolean = false;
  faUserPlusIcon = faUserPlus;
  faUserMinusIcon = faUserMinus;
  faUserEditIcon = faUserEdit;

  workers: Worker[] = [];
  errorCases?: ErrorCase[];
  
  constructor(private modalService: NgbModal, private ValidateTaskForceService: ValidateTaskForceService) { }

  addWorker():void{
    const modalRef = this.modalService.open(AddNewWorkerComponent);
    this.handleModalWorkerEvent(modalRef);
    
  }

  modifyWorker(nam:string):void{
    const modalRef = this.modalService.open(AddNewWorkerComponent);
    modalRef.componentInstance.workerToModify = this.workers.find(w => w.Person.Nam === nam);

    this.handleModalWorkerEvent(modalRef);
  }

  removeWorker(nam:string):void{
      this.workers = this.workers.filter((worker) => {
      return worker.Person.Nam !== nam;
    });

    this.validateTaskForce();
  }

  private handleModalWorkerEvent(modalRef: NgbModalRef){
    modalRef.componentInstance.newWorkerEvent.subscribe( (newWorker: Worker) =>{
      const worker = this.workers.find(w => w.Person.Nam === newWorker.Person.Nam);
      if(worker){
        const index = this.workers.indexOf(worker);
        this.workers[index] = newWorker;
      }else{
        this.workers.push(newWorker);
      }

      this.validateTaskForce();
    });
  }

  private validateTaskForce() : void{
    if(!this.workers.length){
      this.errorCases = undefined;
      return;
    }

    const resultat = this.ValidateTaskForceService.validateTaskForce(this.workers);

    if(!resultat.Success){
      this.errorCases = resultat.Errors;
    }
  }

  public hasUnsavedChanges() : boolean{
    return this.workers.length > 0;
  }

  saveTaskForce():void {
    this.validateTaskForce();
  }
}
