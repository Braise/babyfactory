import { Routes } from "@angular/router";
import { DisplayTaskForceComponent } from "./Components/display-task-force/display-task-force.component";
import { NewTaskForceComponent } from "./Components/new-task-force/new-task-force.component";
import { TaskForceResolverService } from "./Services/task-force-resolver.service";

export const appRoutes: Routes = [
    {
        path: 'taskforce/new',
        component: NewTaskForceComponent,
        canDeactivate: ['canDeactivateCreateTaskForce']
    },
    {
        path: 'taskforce/:id',
        component: DisplayTaskForceComponent,
        resolve:{
            taskForce: TaskForceResolverService
        }
    },
    {
        path: '',
        redirectTo: '/taskforce/new',
        pathMatch: 'full'
    }
]