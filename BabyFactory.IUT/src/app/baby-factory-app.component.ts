import { Component } from "@angular/core";
import { faFolderPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
import { CaptainHaddockService } from "./Services/CaptainHaddock.service";

@Component({
    selector: 'babyFactory-app',
    templateUrl: './baby-factory-app.component.html'
  })
export class BabyFactoryAppComponent{

  faFolderPlusIcon = faFolderPlus;
  faSearch = faSearch;
  captainImage!:string;

    constructor(public captainHaddockService: CaptainHaddockService){
      this.captainImage = captainHaddockService.getCaptainMood();
    }
    
    public toggleCaptain(){
      this.captainHaddockService.theCaptainIsWatching = !this.captainHaddockService.theCaptainIsWatching;
      this.captainImage = this.captainHaddockService.getCaptainMood();
    }
}