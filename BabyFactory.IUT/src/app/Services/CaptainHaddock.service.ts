import { Injectable } from "@angular/core";
import CaptainHaddockCurses from '../../assets/CaptainHaddockCurses.json';

@Injectable()
export class CaptainHaddockService{
    
    public theCaptainIsWatching: boolean = false;

    public getCaptainMood():string{
        if(this.theCaptainIsWatching){
            return '../assets/images/CaptainHaddock_MadMiniature.png';
        }else{
            return '../assets/images/CaptainHaddock_HappyMiniature.png';
        }
    }

    public getCurses():string{
        let curses:string="";

        do{
            const randomInt = Math.floor(Math.random() * CaptainHaddockCurses.curses.length);
            curses += CaptainHaddockCurses.curses[randomInt] + "! ";
        }while(curses.split(" ").length - 1 < 4);

        return curses;
    }
}