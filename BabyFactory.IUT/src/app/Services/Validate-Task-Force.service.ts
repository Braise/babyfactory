import { Injectable } from "@angular/core";
import { ErrorCase } from "../Model/ErrorCase.model";
import { ResultatTaskForceValidation } from "../Model/ResultatTaskForceValidation.model";
import { Sex } from "../Model/Sex.enum";
import { TaskForce } from "../Model/TaskForce.model";
import { Worker } from "../Model/Worker.model";

@Injectable()
export class ValidateTaskForceService{

    public validateTaskForce(workers: Worker[]){
        let errorCases: ErrorCase[] = [];

        if(!this.atLeastOneWomen(workers)){
            errorCases.push(new ErrorCase("The task force must have at least one woman"));
        }

        if(!this.atLeastOneDiagnostic(workers)){
            errorCases.push(new ErrorCase("The task force must have at least one worker with a valid diagnostic"));
        }

        const invalidParentalCapacityErrors = this.validateParentalCapacity(workers);

        if(invalidParentalCapacityErrors){
            errorCases = errorCases.concat(invalidParentalCapacityErrors);
        }

        const invalidSterilizationErrors = this.validateSterilization(workers);

        if(invalidSterilizationErrors){
            errorCases = errorCases.concat(invalidSterilizationErrors);
        }

        let resultat: ResultatTaskForceValidation = new ResultatTaskForceValidation();
        if(errorCases.length){
            resultat.Errors = errorCases;
        }else{
            resultat.TaskForce = new TaskForce(workers);
        }

        return resultat;
    }

    private atLeastOneWomen(workers: Worker[]):boolean {
        return workers.find(w => w.Person.Sex === Sex.Female) !== undefined;
    }

    private atLeastOneDiagnostic(workers: Worker[]):boolean {
        return workers.find(w => w.WasDiagnosedWitInfertility || w.WasDiagnosedWithInabilityToReproduce) !== undefined;
    }

    private validateSterilization(workers: Worker[]) : ErrorCase[]{
        let errorCases: ErrorCase[] = [];

        workers.forEach(w => {
            if(w.Person.HasUndergoneSterilization && w.SterilizationWasVoluntary){
                errorCases.push(new ErrorCase("The sterilization was voluntary", w.Person.Nam));
            }
        });

        return errorCases;
    }

    private validateParentalCapacity(workers: Worker[]) : ErrorCase[]{
        let errorCases: ErrorCase[] = [];

        workers.forEach(w => {
            if(!w.HasParentalCapacity){
                errorCases.push(new ErrorCase("Invalid parental capacity", w.Person.Nam));
            }
        });

        return errorCases;
    }
}