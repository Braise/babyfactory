import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable, of } from 'rxjs';
import { catchError } from "rxjs/operators";
import { Person } from '../Model/Person.model';
import { Sex } from '../Model/Sex.enum';

@Injectable()
export class PersonService {

  constructor(private http: HttpClient) { }

  getPerson(nam: string): Observable<Person[]> { // TODO : find better way than returning an array ugly as hell to the caller!
    return this.http.get<Person[]>('/api/persons?Nam=' + nam);
  }
}