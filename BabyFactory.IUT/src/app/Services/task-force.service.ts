import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { TaskForce } from "../Model/TaskForce.model";

@Injectable()
export class TaskForceService{

    constructor(private http: HttpClient) {
        
    }

    public saveTaskForce(newTaskForce : TaskForce): Observable<TaskForce> {
        let options = { headers: new HttpHeaders({ "content-type": "application/json" }) };
        
        return this.http.post<TaskForce>('/api/taskforces', newTaskForce, options);
    }

    public getTaskForce(id: number): Observable<TaskForce> {
        return this.http.get<TaskForce>('/api/taskforces/' + id);
    }
}