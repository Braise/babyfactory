import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve } from "@angular/router";
import { TaskForceService } from "./task-force.service";

@Injectable()
export class TaskForceResolverService implements Resolve<any>{

    constructor(public taskForceService: TaskForceService){}

    resolve(route: ActivatedRouteSnapshot){
        return this.taskForceService.getTaskForce(route.params['id']);
    }
}