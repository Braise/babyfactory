import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { WorkerDetailComponent } from './Components/worker-detail/worker-detail.component';
import { AddNewWorkerComponent } from './Components/add-new-worker/add-new-worker.component'; 
import { NewTaskForceComponent } from './Components/new-task-force/new-task-force.component';
import { PersonService } from './Services/Person.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ValidateTaskForceService } from './Services/Validate-Task-Force.service';
import { DisplayErrorsComponent } from './Components/display-errors/display-errors.component';
import { AppRoutingModule } from './app-routing.module';
import { BabyFactoryAppComponent } from './baby-factory-app.component';
import { CaptainHaddockService } from './Services/CaptainHaddock.service';
import { DisplayTaskForceComponent } from './Components/display-task-force/display-task-force.component';
import { TaskForceService } from './Services/task-force.service';
import { TaskForceResolverService } from './Services/task-force-resolver.service';

@NgModule({
  declarations: [
    BabyFactoryAppComponent,
    WorkerDetailComponent,
    AddNewWorkerComponent,
    NewTaskForceComponent,
    DisplayErrorsComponent,
    DisplayTaskForceComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    AppRoutingModule
  ],
  providers: [
    PersonService,
    ValidateTaskForceService,
    CaptainHaddockService,
    TaskForceService,
    TaskForceResolverService,
    {
      provide: 'canDeactivateCreateTaskForce',
      useValue: checkDirtyStateNewTaskForce
    }
  ],
  bootstrap: [BabyFactoryAppComponent]
})
export class AppModule { }

export function checkDirtyStateNewTaskForce(component: NewTaskForceComponent){
  if(component.hasUnsavedChanges()){
    return window.confirm("You have not saved this task force, do you really want to cancel?")
  }

  return true;
}
